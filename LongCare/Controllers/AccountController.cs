﻿using LongCare.Areas.LoginService.Models.Mails;
using LongCare.Models;
using LongCare.Models.Account;
using System;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;

namespace LongCare.Controllers
{
    public class AccountController : SurfaceController
    {
        #region 處理登入
        public ActionResult RenderLoginForm(ViewModelBased m)
        {
            var model = new LoginViewModel
            {
                ContentId = m.ContentId,
                ParentId = m.ParentId,
                ReturnPageId = m.ReturnPageId,
                MemberId = m.MemberId
            };

            return PartialView("LoginForm", model);
        }
        public ActionResult SubmitLoginForm(LoginViewModel m)
        {
            //違反規定時
            if (!ModelState.IsValid) return CurrentUmbracoPage();
            //若已有登入帳號時
            IMember currentMember = (m.MemberId != null) ? Services.MemberService.GetById(m.MemberId.Value) : null;
            if (currentMember != null)
            {
                //先把Member logout
                CleanLoginData();
            }
            IMember loginMember = Services.MemberService.GetByUsername(m.AccountName);
            if (loginMember == null)
            {
                ViewBag.ErrorMessage = "查無此帳號，請先註冊帳號。";
                return CurrentUmbracoPage();
            }
            if (!Members.Login(m.AccountName, m.Passoword))
            {
                ViewBag.ErrorMessage = "帳號或密碼錯誤。";
                return CurrentUmbracoPage();
            }
            if (m.RememberMe)
            {
                FormsAuthentication.SetAuthCookie(m.AccountName, true);
            }

            if (m.ReturnPageId != null)
            {
                return RedirectToUmbracoPage(m.ReturnPageId.Value);
            }
            else
            {
                return Redirect("~/");
            }

        }
        #endregion
        #region 處理登出
        public ActionResult SubmitLogout()
        {
            CleanLoginData();
            return Redirect("~/");
        }
        #endregion
        #region 忘記密碼
        public ActionResult RenderForgotPasswordForm(ViewModelBased m)
        {
            var model = new ForgotPasswordViewModel
            {
                ContentId = m.ContentId,
                ParentId = m.ParentId,
                ReturnPageId = m.ReturnPageId
                
            };

            return PartialView("ForgotPasswordForm", model);
        }

        public ActionResult SubmitForgotPasswordForm(ForgotPasswordViewModel m)
        {
            if (!ModelState.IsValid) return CurrentUmbracoPage();
            IMember thisMember = Services.MemberService.GetByEmail(m.Email);
            if(thisMember == null)
            {
                ViewBag.ErrorMessage = string.Format("查無 {0} 之會員資料", m.Email);
                return CurrentUmbracoPage();
            }
            string resetGuid = DateTime.Now.AddMinutes(60).ToString("ddMMyyyyHHmmssFFFF");
            thisMember.SetValue("resetToken", resetGuid);
            Services.MemberService.Save(thisMember);
            var mail = new ResetPasswordMail
            {
                To = thisMember.Email,
                Subject = "重設密碼通知信",
                Title = "重設密碼",
                UserName = thisMember.Name,
                ResetGuid = resetGuid,
                ReturnPageId = m.ReturnPageId.Value,
                Message = m.Messages
            };
            mail.Send();
            TempData["successful"] = true;
            return RedirectToCurrentUmbracoPage();
        }
        #endregion


        #region 私有方法
        private void CleanLoginData()
        {
            TempData.Clear();
            Session.Clear();
            FormsAuthentication.SignOut();
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
        }
        #endregion
    }
}