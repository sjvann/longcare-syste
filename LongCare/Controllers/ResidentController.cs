﻿using LongCare.ModelHelper;
using LongCare.Models;
using LongCare.Models.Resident;
using LongCare.Services;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;
using Umbraco.Web.PublishedModels;

namespace LongCare.Controllers
{
    public class ResidentController : SurfaceController
    {
       
        #region 住民資料維護
        public ActionResult RenderMaintainResidentForm(ViewModelBased m)
        {
            MaintainResidentViewModel model = ViewModelHelper.MapViewModelBased<MaintainResidentViewModel>(m);
            if (m.ContentId != null && m.ContentId.Value != 0)
            {
                IContent content = Services.ContentService.GetById(m.ContentId.Value);
                if (content.ContentType.Alias == Resident.ModelTypeAlias)
                {
                    model.Content = ViewModelHelper.MapToContent(content);
                    model.Contact = ViewModelHelper.MapToContactData(content);
                    model.Person = ViewModelHelper.MapToPerson(content);
                }
                model.MaintainType = EnumMaintainType.u;
            }
            else
            {  
                model.MaintainType = EnumMaintainType.c;
            }

            model.GenderList = GetEnumList.GenderList();
            return PartialView("MaintainResidentForm", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitMaintainResidentForm(MaintainResidentViewModel model)
        {

            if (!ModelState.IsValid) return CurrentUmbracoPage();
            string submitResult = string.Empty;
            MyNodeService _h = new MyNodeService(Services);
            IContent thisContent;
            switch (model.MaintainType)
            {
                case EnumMaintainType.c:
                    string nodeName =_h.GetNodeName(model.ParentId.Value, Resident.ModelTypeAlias, "R", 4);
                    thisContent = _h.GetChildNode(model.ParentId.Value, Resident.ModelTypeAlias, nodeName);
                    submitResult = "新增成功";
                    break;
                case EnumMaintainType.u:
                    thisContent = Services.ContentService.GetById(model.ContentId.Value);
                    submitResult = "更新成功";
                    break;
                default:
                    return CurrentUmbracoPage();
            }
            if(model.Content != null)
            {
                ViewModelHelper.MapFromContent(model.Content, ref thisContent);
            }
            if(model.Contact != null)
            {
                ViewModelHelper.MapFromContact(model.Contact, ref thisContent);
            }
            if(model.Person != null)
            {
                ViewModelHelper.MapFromPerson(model.Person, ref thisContent);
            }
           
            Services.ContentService.SaveAndPublish(thisContent);
            ViewData["submitResult"] = submitResult;
            return (model.ReturnPageId != null) ? RedirectToUmbracoPage(model.ReturnPageId.Value, "submitResult=" + submitResult) : RedirectToCurrentUmbracoPage("submitResult=" + submitResult);
        }

        #endregion
        #region 聯絡人資料維護
        public ActionResult RenderMaintainContactPersonForm(ViewModelBased m)
        {
            MaintainContactPersonViewModel model = ViewModelHelper.MapViewModelBased<MaintainContactPersonViewModel>(m);
            if (m.ContentId != null && m.ContentId.Value != 0)
            {
                IContent content = Services.ContentService.GetById(m.ContentId.Value);
                if (content.ContentType.Alias == ContactPerson.ModelTypeAlias)
                {
                    model.Content = ViewModelHelper.MapToContent(content);
                    model.Contact = ViewModelHelper.MapToContactData(content);
                    model.Person = ViewModelHelper.MapToContactPerson(content);
                }
                model.MaintainType = EnumMaintainType.u;
            }
            else
            {
                model.MaintainType = EnumMaintainType.c;
            }

            model.GenderList = GetEnumList.GenderList();
            model.RelationshipList = GetEnumList.RelationshipList();

            return PartialView("MaintainContactPersonForm", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitMaintainContactPersonForm(MaintainContactPersonViewModel model)
        {

            if (!ModelState.IsValid) return CurrentUmbracoPage();
            string submitResult = string.Empty;
            MyNodeService _h = new MyNodeService(Services);
            IContent thisContent;
            switch (model.MaintainType)
            {
                case EnumMaintainType.c:
                    string nodeName = _h.GetNodeName(model.ParentId.Value,  ContactPerson.ModelTypeAlias, true, 2);
                    thisContent = _h.GetChildNode(model.ParentId.Value, ContactPerson.ModelTypeAlias, nodeName);
                    submitResult = "新增成功";
                    break;
                case EnumMaintainType.u:
                    thisContent = Services.ContentService.GetById(model.ContentId.Value);
                    submitResult = "更新成功";
                    break;
                default:
                    return CurrentUmbracoPage();
            }
            if (model.Content != null)
            {
                ViewModelHelper.MapFromContent(model.Content, ref thisContent);
            }
            if (model.Contact != null)
            {
                ViewModelHelper.MapFromContact(model.Contact, ref thisContent);
            }
            if (model.Person != null)
            {
                ViewModelHelper.MapFromContactPerson(model.Person, ref thisContent);
            }

            Services.ContentService.SaveAndPublish(thisContent);

            return (model.ReturnPageId != null) ? RedirectToUmbracoPage(model.ReturnPageId.Value, "submitResult=" + submitResult) : RedirectToCurrentUmbracoPage("submitResult=" + submitResult);
        }

        #endregion

        #region 刪除
        public ActionResult DeleteContent(ViewModelBased model)
        {
            string submitResult = string.Empty;
            if(model.ContentId != 0)
            {
                var c = Services.ContentService.GetById(model.ContentId.Value);
                var result = Services.ContentService.Delete(c);
                if(result.Success)
                {
                    submitResult = "刪除成功";
                }
                else
                {
                    submitResult = "刪除失敗";
                }

            }
            return (model.ReturnPageId != null) ? RedirectToUmbracoPage(model.ReturnPageId.Value, "submitResult=" + submitResult) : RedirectToCurrentUmbracoPage("submitResult=" + submitResult);
        }
        #endregion
    }
}