﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.PublishedModels;
using Umbraco.Core;


namespace LongCare.Services
{
    public class MyNodeServicebak
    {
        private static MyNodeService _instance;
        private static object syncLock = new object();
        private readonly IContentService _cs;
        private readonly IMediaService _ms;
        private readonly UmbracoHelper _help;
        private readonly ServiceContext _s;

        protected MyNodeService()
        {
            if(_s == null)
            {
                _s = Umbraco.Web.Composing.Current.Services;
                _cs = Umbraco.Web.Composing.Current.Services.ContentService;
                _ms = Umbraco.Web.Composing.Current.Services.MediaService;
              
            }
            if(_help == null)
            {
                _help = Umbraco.Web.Composing.Current.UmbracoHelper;
            }
        }
        public static MyNodeService GetService()
        {
            if (_instance == null)
            {
                lock (syncLock)
                {
                    if (_instance == null)
                    {
                        _instance = new MyNodeService();
                    }
                }
            }
            return _instance;
        }
     

        #region 根目錄

        public IContent GetRootNode(string contentTypeAlias, string nodeName = null)
        {
            IContent root = null;
            if (nodeName != null)
            {
                root = _cs.GetRootContent().FirstOrDefault(x => x.ContentType.Alias == contentTypeAlias && x.Name == nodeName);
            }
            else
            {
                root = _cs.GetRootContent().FirstOrDefault(x => x.ContentType.Alias == contentTypeAlias);
            }
            if (root == null)
            {
                root = _cs.CreateAndSave(nodeName ?? contentTypeAlias, -1, contentTypeAlias);
            }
            if (!root.Published)
            {
                _cs.SaveAndPublish(root);
            }
            return root;
        }
        public T GetRootPublishedNode<T>(string contentTypeAlias, string nodeName = null) where T : PublishedContentModel
        {
            IContent root = GetRootNode(contentTypeAlias, nodeName);
            return (T)_help.Content(root.Id);
        }
        #endregion
        #region 檢查子節點
        public IContent CheckChildContent(int parentId, string childContenetTypeAlias, string childNodeName)
        {

            var root = _cs.GetById(parentId);
            if (root == null) throw new ArgumentException("不能沒有父節點");
            int pageSize = 100;

            IEnumerable<IContent> children = _cs.GetPagedChildren(root.Id, 0, pageSize, out long totalChildren);
            IContent child = children.FirstOrDefault(x => x.ContentType.Alias == childContenetTypeAlias && x.Name == childNodeName);
            if (child == null && (int)totalChildren >= pageSize)
            {
                children = _cs.GetPagedChildren(root.Id, 1, (int)totalChildren - pageSize, out totalChildren);
                child = children.FirstOrDefault(x => x.ContentType.Alias == childContenetTypeAlias && x.Name == childNodeName);
            }
            else if (child == null && (int)totalChildren < pageSize)
            {
                child = _cs.Create(childNodeName, root.Id, childContenetTypeAlias);
            }
            return child;
        }
        public IMedia CheckChildMedia(int parentFolderId, string medialTypeAlias, string mediaName)
        {
            var folder = _ms.GetById(parentFolderId);
            if (folder == null) throw new ArgumentException("不能沒有父節點");
            int pageSize = 100;

            IEnumerable<IMedia> children = _ms.GetPagedChildren(folder.Id, 0, pageSize, out long totalChildren);
            IMedia child = children.FirstOrDefault(x => x.ContentType.Alias == medialTypeAlias && x.Name == mediaName);
            if (child == null && (int)totalChildren >= pageSize)
            {
                children = _ms.GetPagedChildren(folder.Id, 1, (int)totalChildren - pageSize, out totalChildren);
                child = children.FirstOrDefault(x => x.ContentType.Alias == medialTypeAlias && x.Name == mediaName);
            }
            else if (child == null && (int)totalChildren < pageSize)
            {
                child = _ms.CreateMedia(mediaName, folder.Id, medialTypeAlias);
            }
            return child;
        }
        #endregion
        #region 特定子節點
        public IContent GetChildNode(int parentId, string childContenetTypeAlias, string childNodeName = null, string pageTitle = null)
        {

            IContent child = CheckChildContent(parentId, childContenetTypeAlias, childNodeName);
            if (child != null)
            {
                if (child.HasProperty("pageTitle") && child.GetValue("pageTitle") == null)
                {
                    child.SetValue("pageTitle", pageTitle ?? childNodeName);
                }
                else if(child.HasProperty("title") && child.GetValue("title") == null)
                {
                    child.SetValue("title", pageTitle ?? childNodeName);
                }
                _cs.SaveAndPublish(child);
            }

            return child;
        }
        public T GetChildPublishedNode<T>(int parentId, string childContenetTypeAlias, string childNodeName, string pageTitle = null) where T : PublishedContentModel
        {
            var child = this.GetChildNode(parentId, childContenetTypeAlias, childNodeName, pageTitle);
            return (T)_help.Content(child.Id);
        }


        #endregion
        #region 建立Node名稱
        public string GetNodeName(int parentId, string childContenetTypeAlias, string prefix, int length)
        {
            int childCount = _cs.CountChildren(parentId) + 1;
            string s = Math.Pow(10, length).ToString();
            string right = s.Substring(s.Length - length, length);
            string name;
            bool noChild;
            do
            {
                name = prefix + childCount.ToString(right);
                IContent child = CheckChildContent(parentId, childContenetTypeAlias, name);
                if (child != null)
                {
                    childCount++;
                    noChild = false;
                }
                else
                {
                    noChild = true;
                }
            } while (noChild);

            return name;
        }
        public string GetNodeName(int parentId, string childContenetTypeAlias, bool addParent, int length)
        {
            if(addParent)
            {
                IContent p = _cs.GetById(parentId);
                return this.GetNodeName(parentId, childContenetTypeAlias, p.Name, length);
            }
            else
            {
                return this.GetNodeName(parentId, childContenetTypeAlias, String.Empty, length);
            }
           
        }
        #endregion
        #region 媒體類
        public IMedia GetMediaFolderByName(string folderName)
        {
            var folder = _ms.GetRootMedia().FirstOrDefault(x => x.Name == folderName && x.ContentType.Alias == Folder.ModelTypeAlias) ??
                _ms.CreateMedia(folderName, -1, Umbraco.Core.Constants.Conventions.MediaTypes.Folder);
            _ms.Save(folder);
            return folder;
        }

        public IMedia GetMediaFileByName(int parentFolderId, string MediaTypeAlias, HttpPostedFileBase attachment)
        {
            IMedia medial = CheckChildMedia(parentFolderId, MediaTypeAlias, attachment.FileName);

            medial.SetValue(_s.ContentTypeBaseServices, Constants.Conventions.Media.File, attachment.FileName, attachment.InputStream);
           
            _ms.Save(medial);

            return medial;
        }
        public IPublishedContent GetMediaIdByUdi(Udi udi)
        {
            var t = _help?.Media(udi);
            return t;
        }
        #endregion
    }
}