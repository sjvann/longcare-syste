﻿using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Core.PropertyEditors;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace LongCare.Services
{
    public class MyDataTypeService
    {
        private static MyDataTypeService _instance;
        private static object syncLock = new object();
        private readonly IDataTypeService _ds;
        private readonly UmbracoHelper _help;

        protected MyDataTypeService()
        {
            if(_ds == null)
            {
                _ds = Umbraco.Web.Composing.Current.Services.DataTypeService;
            }
            if(_help == null)
            {
                _help = Umbraco.Web.Composing.Current.UmbracoHelper;
            }
        }
        public static MyDataTypeService GetService()
        {
            if(_instance == null)
            {
               lock (syncLock)
                {
                    if(_instance == null)
                    {
                        _instance = new MyDataTypeService();
                    }
                }
            }
            return _instance;
        }
        /// <summary>
        /// 從字串取得該資料型態的ID
        /// </summary>
        /// <param name="preValue">欲分析之字串</param>
        /// <param name="dataTypeAliase">資料型態名稱</param>
        /// <returns></returns>
        public int GetDataTypeIdByString(string preValue, string dataTypeAliase)
        {
            int dataItemId = 0;
            IDataType dataType = _ds.GetDataType(dataTypeAliase);
            if(dataType != null)
            {
                ValueListConfiguration valueList =(ValueListConfiguration)dataType.Configuration;
                if(valueList != null && valueList.Items != null && valueList.Items.Any())
                {
                    dataItemId = (from d in valueList.Items
                                  where d.Value == preValue
                                  select d.Id).First();
                }

            }
            return dataItemId;
        }

    }
}