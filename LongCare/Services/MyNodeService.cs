﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Services;

using Umbraco.Web.PublishedModels;
using Umbraco.Core;
using Umbraco.Web;

namespace LongCare.Services
{
    public class MyNodeService
    {
        private readonly ServiceContext _s;
        private readonly UmbracoHelper _help;
        public  MyNodeService(ServiceContext service)
        {
            _s = service;
            _help = Umbraco.Web.Composing.Current.UmbracoHelper;

        }
        #region 根目錄

        public IContent GetRootNode(string contentTypeAlias, string nodeName = null)
        {
            IContent root = null;
            if (nodeName != null)
            {
                root = _s.ContentService.GetRootContent().FirstOrDefault(x => x.ContentType.Alias == contentTypeAlias && x.Name == nodeName);
            }
            else
            {
                root = _s.ContentService.GetRootContent().FirstOrDefault(x => x.ContentType.Alias == contentTypeAlias);
            }
            if (root == null)
            {
                root = _s.ContentService.CreateAndSave(nodeName ?? contentTypeAlias, -1, contentTypeAlias);
            }
            if (!root.Published)
            {
                _s.ContentService.SaveAndPublish(root);
            }
            return root;
        }
        public T GetRootPublishedNode<T>(string contentTypeAlias, string nodeName = null) where T : PublishedContentModel
        {
            IContent root = GetRootNode(contentTypeAlias, nodeName);
            return (T)_help.Content(root.Id);
        }
        #endregion
        #region 檢查子節點
        public IContent CheckChildContent(int parentId, string childContenetTypeAlias, string childNodeName)
        {

            var root = _s.ContentService.GetById(parentId);
            if (root == null) throw new ArgumentException("不能沒有父節點");
            int pageSize = 100;

            IEnumerable<IContent> children = _s.ContentService.GetPagedChildren(root.Id, 0, pageSize, out long totalChildren);
            IContent child = children.FirstOrDefault(x => x.ContentType.Alias == childContenetTypeAlias && x.Name == childNodeName);
            if (child == null && (int)totalChildren >= pageSize)
            {
                children = _s.ContentService.GetPagedChildren(root.Id, 1, (int)totalChildren - pageSize, out totalChildren);
                child = children.FirstOrDefault(x => x.ContentType.Alias == childContenetTypeAlias && x.Name == childNodeName);
            }
            else if (child == null && (int)totalChildren < pageSize)
            {
                child = _s.ContentService.Create(childNodeName, root.Id, childContenetTypeAlias);
            }
            return child;
        }
        public IMedia CheckChildMedia(int parentFolderId, string medialTypeAlias, string mediaName)
        {
            var folder = _s.MediaService.GetById(parentFolderId);
            if (folder == null) throw new ArgumentException("不能沒有父節點");
            int pageSize = 100;

            IEnumerable<IMedia> children = _s.MediaService.GetPagedChildren(folder.Id, 0, pageSize, out long totalChildren);
            IMedia child = children.FirstOrDefault(x => x.ContentType.Alias == medialTypeAlias && x.Name == mediaName);
            if (child == null && (int)totalChildren >= pageSize)
            {
                children = _s.MediaService.GetPagedChildren(folder.Id, 1, (int)totalChildren - pageSize, out totalChildren);
                child = children.FirstOrDefault(x => x.ContentType.Alias == medialTypeAlias && x.Name == mediaName);
            }
            else if (child == null && (int)totalChildren < pageSize)
            {
                child = _s.MediaService.CreateMedia(mediaName, folder.Id, medialTypeAlias);
            }
            return child;
        }
        #endregion
        #region 特定子節點
        public IContent GetChildNode(int parentId, string childContenetTypeAlias, string childNodeName = null, string pageTitle = null)
        {

            IContent child = CheckChildContent(parentId, childContenetTypeAlias, childNodeName);
            if (child != null)
            {
                if (child.HasProperty("pageTitle") && child.GetValue("pageTitle") == null)
                {
                    child.SetValue("pageTitle", pageTitle ?? childNodeName);
                }
                else if(child.HasProperty("title") && child.GetValue("title") == null)
                {
                    child.SetValue("title", pageTitle ?? childNodeName);
                }
                _s.ContentService.SaveAndPublish(child);
            }

            return child;
        }
        public T GetChildPublishedNode<T>(int parentId, string childContenetTypeAlias, string childNodeName, string pageTitle = null) where T : PublishedContentModel
        {
            var child = this.GetChildNode(parentId, childContenetTypeAlias, childNodeName, pageTitle);
            return (T)_help.Content(child.Id);
        }


        #endregion
        #region 建立Node名稱
        public string GetNodeName(int parentId, string childContenetTypeAlias, string prefix, int length)
        {
            int childCount = _s.ContentService.CountChildren(parentId) + 1;
            string s = Math.Pow(10, length).ToString();
            string right = s.Substring(s.Length - length, length);
            string name;
            bool noChild;
            do
            {
                name = prefix + childCount.ToString(right);
                IContent child = CheckChildContent(parentId, childContenetTypeAlias, name);
                if (child != null)
                {
                    childCount++;
                    noChild = false;
                }
                else
                {
                    noChild = true;
                }
            } while (noChild);

            return name;
        }
        public string GetNodeName(int parentId, string childContenetTypeAlias, bool addParent, int length)
        {
            if(addParent)
            {
                IContent p = _s.ContentService.GetById(parentId);
                return this.GetNodeName(parentId, childContenetTypeAlias, p.Name, length);
            }
            else
            {
                return this.GetNodeName(parentId, childContenetTypeAlias, String.Empty, length);
            }
           
        }
        #endregion
        #region 媒體類
        public IMedia GetMediaFolderByName(string folderName)
        {
            var folder = _s.MediaService.GetRootMedia().FirstOrDefault(x => x.Name == folderName && x.ContentType.Alias == Folder.ModelTypeAlias) ??
                _s.MediaService.CreateMedia(folderName, -1, Umbraco.Core.Constants.Conventions.MediaTypes.Folder);
            _s.MediaService.Save(folder);
            return folder;
        }

        public IMedia GetMediaFileByName(int parentFolderId, string MediaTypeAlias, HttpPostedFileBase attachment)
        {
            IMedia medial = CheckChildMedia(parentFolderId, MediaTypeAlias, attachment.FileName);

            medial.SetValue(_s.ContentTypeBaseServices, Constants.Conventions.Media.File, attachment.FileName, attachment.InputStream);
           
            _s.MediaService.Save(medial);

            return medial;
        }
        public IPublishedContent GetMediaIdByUdi(Udi udi)
        {
            var t = _help?.Media(udi);
            return t;
        }
        #endregion
    }
}