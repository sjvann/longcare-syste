﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace LongCare.Services
{
    #region Enum主體
    public enum EnumMaintainType
    {
        c, r, u, d, e
    }
    public enum EnumGenderType
    {
        Female, Male, Unknow
    }
    public enum EnumKinship
    {
        son, daughter, father, mother, husband, wife, borther, sister, grandfather, grandmother, grandson, granddaughter, others
    }

    #endregion
    #region 產生中文內容
    public static class EnumTypeExtensionForChineseString
    {
        public static string ToChineseString(this EnumKinship t)
        {
            switch (t)
            {
                case EnumKinship.son:
                    return "兒子";
                case EnumKinship.daughter:
                    return "女兒";
                case EnumKinship.father:
                    return "父親";
                case EnumKinship.mother:
                    return "母親";
                case EnumKinship.husband:
                    return "丈夫";
                case EnumKinship.wife:
                    return "妻子";
                case EnumKinship.borther:
                    return "兄弟";
                case EnumKinship.sister:
                    return "姐妹";
                case EnumKinship.grandfather:
                    return "祖父";
                case EnumKinship.grandmother:
                    return "祖母";
                case EnumKinship.grandson:
                    return "孫兒";
                case EnumKinship.granddaughter:
                    return "孫女";
                default:
                    return "未知";
            }
        }
        public static string ToChineseString(this EnumMaintainType t)
        {
            switch (t)
            {
                case EnumMaintainType.c:
                    return "新增";
                case EnumMaintainType.r:
                    return "讀取";
                case EnumMaintainType.u:
                    return "更新";
                case EnumMaintainType.d:
                    return "刪除";
                case EnumMaintainType.e:
                    return "錯誤";
                default:
                    return "未知";
            }
        }
        public static string ToChineseString(this EnumGenderType t)
        {
            switch (t)
            {
                case EnumGenderType.Female:
                    return "女性";
                case EnumGenderType.Male:
                    return "男性";
                case EnumGenderType.Unknow:
                    return "未知";
                default:
                    return "未知";

            }
        }
    }
    #endregion

    #region 產生清單
    public static class GetEnumList
    {   public static IEnumerable<SelectListItem> GenderList()
        {
            IList<SelectListItem> list = new List<SelectListItem>();
            foreach (EnumGenderType t in Enum.GetValues(typeof(EnumGenderType)))
            {
                list.Add(new SelectListItem
                {
                    Value = t.ToString(),
                    Text = t.ToChineseString()
                });
            }
            return list;
        }
        public static IEnumerable<SelectListItem> RelationshipList()
        {
            IList<SelectListItem> list = new List<SelectListItem>();
            foreach (EnumKinship t in Enum.GetValues(typeof(EnumKinship)))
            {
                list.Add(new SelectListItem
                {
                    Value = t.ToString(),
                    Text = t.ToChineseString()
                });
            }
            return list;
        }
    }

    #endregion
}