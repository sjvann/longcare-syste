﻿using LongCare.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace LongCare.ModelHelper
{
    public class DataTypeMapper
    {
        private readonly MyNodeService _h = new MyNodeService(Umbraco.Core.Composing.Current.Services);
        public DataTypeMapper()
        {

        }


        #region 列舉資料型態
        public static T MapToMyEnum<T>(IContent content, string fieldName)
        {

            T enumTemp = (T)Activator.CreateInstance(typeof(T));
            if(content != null)
            {
                string enumString = content.GetValue<string>(fieldName);
                if(enumString != null)
                {
                    enumTemp = (T)Enum.Parse(typeof(T), JArray.Parse(enumString).First.Value<string>());
                }
            }
            return enumTemp;
        }
        public static string MapFromMyEnum<T>(T t)
        {
            string enuItem = "[\"" + t.ToString() + "\"]";
            return enuItem;
        }
        #endregion
        #region 多媒體類資料型態

        public IPublishedContent MapToAttachement(IContent content, string fieldName)
        {
            Udi udi = content.GetValue<Udi>(fieldName);

            return _h.GetMediaIdByUdi(udi);
        }

        public string MapFromAttachment(string folderName, string medialTypeAlias, HttpPostedFileBase file)
        {
            IMedia rootFolder = _h.GetMediaFolderByName(folderName);
            if (file != null && file.ContentLength > 0)
            {
               
                IMedia mediaFile = _h.GetMediaFileByName(rootFolder.Id, medialTypeAlias, file);
                
                var udiMedia = Udi.Create(Constants.UdiEntityType.Media, mediaFile.Key);
                return udiMedia.ToString();
            }

            return string.Empty;
        }
        #endregion
    }
}