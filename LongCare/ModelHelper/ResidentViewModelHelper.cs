﻿using LongCare.Models.Interface;
using LongCare.Models.Resident;


namespace LongCare.ModelHelper
{
    public class ResidentViewModelHelper
    {

        readonly MaintainResidentViewModel _model;
        public ResidentViewModelHelper(IViewModelBased m)
        {
            _model = ViewModelHelper.MapViewModelBased<MaintainResidentViewModel>(m);
            
        }
      
    }
}