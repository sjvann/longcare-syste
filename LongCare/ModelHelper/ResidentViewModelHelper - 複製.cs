﻿using LongCare.Models.Composition;
using LongCare.Models.Interface;
using LongCare.Models.Resident;
using Umbraco.Core.Models;
namespace LongCare.ModelHelper
{
    public static class ResidentViewModelHelperBack
    {
        #region 從資料來源對應至ViewModel


        /// <summary>
        /// 從資料來源對應到ViewModel
        /// </summary>
        /// <param name="c">資料來源</param>
        /// <param name="m">ViewMode</param>
        public static void MapResident2ViewModel(IContent c, out MaintainResidentViewModel m)
        {

            if (c != null && c.ContentType.Alias == Umbraco.Web.PublishedModels.Resident.ModelTypeAlias)
            {
                MaintainResidentViewModel _m = new MaintainResidentViewModel();
                IContentInformation pi = ViewModelHelper.MapPersonInformation<PersonInformation>(c);
                _m.Person = new PersonInformation
                {
                    PersonID = pi.PersonID,
                    RealName = pi.RealName,
                    NickName = pi.NickName,
                    Gender = pi.Gender,
                    Birthday = pi.Birthday,
                    PictureUrl = pi.PictureUrl
                };

                IContactInformation cd = ViewModelHelper.MapContactInformation<ContactInformation>(c);
                _m.Contact = new ContactInformation
                {
                    Tel_Home = cd.Tel_Home,
                    Tel_Office = cd.Tel_Office,
                    Tel_Mobile = cd.Tel_Mobile,
                    Email = cd.Email
                };
                IContentInformation ci = ViewModelHelper.MapContentInformation<ContentInformation>(c);
                _m.Content = new ContentInformation
                {
                    Title = ci.Title,
                    Description = ci.Description
                };
                m = _m;
            }
            else
            {
                m = null;
            }
        }
        #endregion
        #region 從ViewModel對應要資料源
        public static void MapResident2Content(MaintainResidentViewModel m, ref IContent c)
        {
            if (m != null && c != null)
            {  
                if(m.Person != null)
                {
                    ViewModelHelper.MapContentFromPerson(m.Person, ref c);
                }
                if(m.Contact != null)
                {
                    ViewModelHelper.MapContentFromContact(m.Contact, ref c);
                }
               
                if(m.Content != null)
                {
                    ViewModelHelper.MapContentFromContent(m.Content, ref c);
                }
          
              
            }

        }
        #endregion
    }
}