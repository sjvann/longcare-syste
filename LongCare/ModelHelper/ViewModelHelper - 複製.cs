﻿using LongCare.Models.Interface;
using LongCare.Services;
using Newtonsoft.Json.Linq;
using System;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.PublishedModels;
using Content = Umbraco.Web.PublishedModels.Content;

namespace LongCare.ModelHelper
{
    public static class ViewModelHelper
    {
        public static T MapViewModelBased<T>(IViewModelBased m) where T: IViewModelBased
        {
            T model = (T)Activator.CreateInstance(typeof(T));
            if(m.ContentId != null)
            {
                model.ContentId = m.ContentId;
                model.ParentId = m.ParentId;
                model.ReturnPageId = m.ReturnPageId;
                model.MemberId = m.MemberId;
                model.SiteId = m.SiteId;
            }
            return model;
        }

        public static T MapContent<T>(IContent m) where T : IContentInformation
        {

            T model = (T)Activator.CreateInstance(typeof(T));
            model.Title = m.GetValue<string>(Content.GetModelPropertyType(f => f.Title).Alias);
            model.Description = m.GetValue<string>(Content.GetModelPropertyType(f => f.Description).Alias);
            //model.MainImage=

            return model;
        }


        #region 從Content對應回ViewModel
        public static T MapContentInformation<T>(IContent c) where T : IContentInformation
        {
            IContentInformation model = (T)Activator.CreateInstance(typeof(T));
            model.Title = c.GetValue<string>(ContentControls.GetModelPropertyType(f => f.Title).Alias);
            model.Description = c.GetValue<string>(ContentControls.GetModelPropertyType(f => f.Description).Alias);
            return (T)model;
        }
        public static T MapPersonInformation<T>(IContent c) where T : IContentInformation
        {
            IContentInformation model = (T)Activator.CreateInstance(typeof(T));
            model.PersonID = c.GetValue<string>(PersonControls.GetModelPropertyType(x => x.PersonID).Alias);
            model.RealName = c.GetValue<string>(PersonControls.GetModelPropertyType(x => x.RealName).Alias);
            model.NickName = c.GetValue<string>(PersonControls.GetModelPropertyType(x => x.NickName).Alias);
            string jgender = c.GetValue<string>("gender");
            if (jgender != null)
            {
                model.Gender = (EnumGenderType)Enum.Parse(typeof(EnumGenderType), JArray.Parse(jgender).First.Value<string>());

            }
            model.Birthday = c.GetValue<DateTime>(PersonControls.GetModelPropertyType(x => x.Birthday).Alias);
            var p = c.GetValue<IPublishedContent>((PersonControls.GetModelPropertyType(x => x.Picture).Alias));
            if (p != null)
            {
                model.PictureId = p.Id;
                model.PictureUrl = p.Url;
            }

            return (T)model;
        }

        public static T MapContactInformation<T>(IContent c) where T : IContactInformation
        {
            IContactInformation model = (T)Activator.CreateInstance(typeof(T));
            model.Tel_Home = c.GetValue<string>(ContactDataControls.GetModelPropertyType(x => x.TelHome).Alias);
            model.Tel_Office = c.GetValue<string>(ContactDataControls.GetModelPropertyType(x => x.TelOffice).Alias);
            model.Tel_Mobile = c.GetValue<string>(ContactDataControls.GetModelPropertyType(x => x.TelMobile).Alias);
            model.Email = c.GetValue<string>(ContactDataControls.GetModelPropertyType(x => x.EMail).Alias);
            return (T)model;
        }
        #endregion
        #region 從ViewModel對應回Content
        public static void MapContentFromPerson(IContentInformation vm, ref IContent c)
        {
            c.SetValue(PersonControls.GetModelPropertyType(x => x.PersonID).Alias, vm.PersonID);
            c.SetValue(PersonControls.GetModelPropertyType(x => x.RealName).Alias, vm.RealName);
            c.SetValue(PersonControls.GetModelPropertyType(x => x.NickName).Alias, vm.NickName);
            c.SetValue(PersonControls.GetModelPropertyType(x => x.Birthday).Alias, vm.Birthday);

            string gender = "[\"" + vm.Gender.ToString() + "\"]";

            c.SetValue(PersonControls.GetModelPropertyType(x => x.Gender).Alias, gender);
            if (vm.Attachment != null && vm.Attachment.ContentLength > 0)
            {
                IMedia personFolder = MyNodeService.GetService().GetMediaFolderByName("PersonPicture");
                IMedia picture = MyNodeService.GetService().GetMediaFileByName(personFolder.Id, Image.ModelTypeAlias, vm.Attachment);
                var udiMedia = Udi.Create(Constants.UdiEntityType.Media, picture.Key);
                c.SetValue(PersonControls.GetModelPropertyType(f => f.Picture).Alias, udiMedia.ToString());

            }
        }
        public static void MapContentFromContact(IContactInformation vm, ref IContent c)
        {

            c.SetValue(ContactDataControls.GetModelPropertyType(x => x.TelHome).Alias, vm.Tel_Home);
            c.SetValue(ContactDataControls.GetModelPropertyType(x => x.TelOffice).Alias, vm.Tel_Office);
            c.SetValue(ContactDataControls.GetModelPropertyType(x => x.TelMobile).Alias, vm.Tel_Mobile);
            c.SetValue(ContactDataControls.GetModelPropertyType(x => x.EMail).Alias, vm.Email);

        }
        public static void MapContentFromContent(IContentInformation vm, ref IContent c)
        {
            if (vm.Title != null)
            {
                c.SetValue(ContentControls.GetModelPropertyType(f => f.Title).Alias, vm.Title);

            }
            if (vm.Description != null)
            {
                c.SetValue(ContentControls.GetModelPropertyType(f => f.Description).Alias, vm.Description);

            }
        }
        #endregion
    }
}