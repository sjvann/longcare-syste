﻿using LongCare.Models.Interface;
using LongCare.Models.Composition;
using LongCare.Services;
using System;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.PublishedModels;
using LongCare.Models;
using Umbraco.Core;

namespace LongCare.ModelHelper
{
    public static class ViewModelHelper
    {
        public static T MapViewModelBased<T>(IViewModelBased m) where T : ViewModelBased
        {
            T model = (T)Activator.CreateInstance(typeof(T));
            if (m.ContentId != null)
            {
                model.ContentId = m.ContentId;
                model.ParentId = m.ParentId;
                model.ReturnPageId = m.ReturnPageId;
                model.MemberId = m.MemberId;
                model.SiteId = m.SiteId;
            }
            return model;
        }
        #region 從Content對應到ViewModel
        public static ContentInformation MapToContent(IContent m)
        {
            ContentInformation model = new ContentInformation
            {
                Title = m.GetValue<string>(ContentControls.GetModelPropertyType(f => f.Title).Alias),
                //MainImage
                Description = m.GetValue<string>(ContentControls.GetModelPropertyType(f => f.Description).Alias)

            };

            return model;
        }
        public static PersonInformation MapToPerson(IContent m)
        {

            PersonInformation model = new PersonInformation
            {
                PersonID = m.GetValue<string>(PersonControls.GetModelPropertyType(x => x.PersonID).Alias),
                RealName = m.GetValue<string>(PersonControls.GetModelPropertyType(f => f.RealName).Alias),
                NickName = m.GetValue<string>(PersonControls.GetModelPropertyType(f => f.NickName).Alias),
                Birthday = m.GetValue<DateTime>(PersonControls.GetModelPropertyType(x => x.Birthday).Alias),
                Gender = DataTypeMapper.MapToMyEnum<EnumGenderType>(m, PersonControls.GetModelPropertyType(f => f.Gender).Alias),
                
            };

            IPublishedContent p = new DataTypeMapper().MapToAttachement(m, PersonControls.GetModelPropertyType(f => f.Picture).Alias);
            if (p != null)
            {
                model.PictureId = p.Id;
                model.PictureUrl = p.Url;
            }
            return model;
        }
        public static ContactPersonInformation MapToContactPerson(IContent m)
        {

            ContactPersonInformation model = new ContactPersonInformation
            {
                RealName = m.GetValue<string>(ContactPersonControls.GetModelPropertyType(f => f.RealName).Alias),
                NickName = m.GetValue<string>(ContactPersonControls.GetModelPropertyType(f => f.NickName).Alias),
                Gender = DataTypeMapper.MapToMyEnum<EnumGenderType>(m, ContactPersonControls.GetModelPropertyType(f => f.Gender).Alias),
                Relationship = DataTypeMapper.MapToMyEnum<EnumKinship>(m, ContactPersonControls.GetModelPropertyType(f => f.Relationship).Alias)
            };

            return model;
        }
        public static ContactDataInformation MapToContactData(IContent m)
        {
            ContactDataInformation model = new ContactDataInformation
            {
                Tel_Home = m.GetValue<string>(ContactDataControls.GetModelPropertyType(f => f.TelHome).Alias),
                Tel_Office = m.GetValue<string>(ContactDataControls.GetModelPropertyType(f => f.TelOffice).Alias),
                Tel_Mobile = m.GetValue<string>(ContactDataControls.GetModelPropertyType(f => f.TelMobile).Alias),
                Email = m.GetValue<string>(ContactDataControls.GetModelPropertyType(f => f.EMail).Alias)
            };
            return model;
        }
        #endregion

        #region 從ViewModel對應到Content
        public static void MapFromContent(ContentInformation source, ref IContent content)
        {
            if (source.Title != null)
            {
                content.SetValue(ContentControls.GetModelPropertyType(f => f.Title).Alias, source.Title);
            }
            if (source.Description != null)
            {
                content.SetValue(ContentControls.GetModelPropertyType(f => f.Description).Alias, source.Description);
            }
        }
        public static void MapFromPerson(PersonInformation source, ref IContent content)
        {
            DataTypeMapper _m = new DataTypeMapper();
            if (source.PersonID != null)
            {
                content.SetValue(PersonControls.GetModelPropertyType(f => f.PersonID).Alias, source.PersonID);
            }
            if (source.RealName != null)
            {
                content.SetValue(PersonControls.GetModelPropertyType(f => f.RealName).Alias, source.RealName);
            }
            if (source.NickName != null)
            {
                content.SetValue(PersonControls.GetModelPropertyType(f => f.NickName).Alias, source.NickName);
            }
            if (source.Birthday.Year > 1000)
            {
                content.SetValue(PersonControls.GetModelPropertyType(f => f.Birthday).Alias, source.Birthday);
            }
            content.SetValue(PersonControls.GetModelPropertyType(f => f.Gender).Alias, DataTypeMapper.MapFromMyEnum<EnumGenderType>(source.Gender));
            if(source.Attachment != null)
            {
                string pictureUdi = _m.MapFromAttachment("PersonPicture", Image.ModelTypeAlias, source.Attachment);
                if (!string.IsNullOrEmpty(pictureUdi))
                {
                    content.SetValue(PersonControls.GetModelPropertyType(f => f.Picture).Alias, pictureUdi);
                }
            }
            else
            {
                IPublishedContent p = _m.MapToAttachement(content, PersonControls.GetModelPropertyType(f => f.Picture).Alias);
                Udi udiMedia = Udi.Create(Constants.UdiEntityType.Media, p.Key);
                content.SetValue(PersonControls.GetModelPropertyType(f => f.Picture).Alias, udiMedia);
            }
        }
        public static void MapFromContactPerson(ContactPersonInformation source, ref IContent content)
        {
          
            if (source.RealName != null)
            {
                content.SetValue(ContactPersonControls.GetModelPropertyType(f => f.RealName).Alias, source.RealName);
            }
            if (source.NickName != null)
            {
                content.SetValue(ContactPersonControls.GetModelPropertyType(f => f.NickName).Alias, source.NickName);
            }
            content.SetValue(ContactPersonControls.GetModelPropertyType(f => f.Gender).Alias, DataTypeMapper.MapFromMyEnum<EnumGenderType>(source.Gender));
            content.SetValue(ContactPersonControls.GetModelPropertyType(f => f.Relationship).Alias, DataTypeMapper.MapFromMyEnum<EnumKinship>(source.Relationship));
        }
        public static void MapFromContact(ContactDataInformation source, ref IContent content)
        {
            if (source.Tel_Home != null)
            {
                content.SetValue(ContactDataControls.GetModelPropertyType(f => f.TelHome).Alias, source.Tel_Home);
            }
            if (source.Tel_Office != null)
            {
                content.SetValue(ContactDataControls.GetModelPropertyType(f => f.TelOffice).Alias, source.Tel_Office);
            }
            if (source.Tel_Mobile != null)
            {
                content.SetValue(ContactDataControls.GetModelPropertyType(f => f.TelMobile).Alias, source.Tel_Mobile);
            }
            if (source.Email != null)
            {
                content.SetValue(ContactDataControls.GetModelPropertyType(f => f.EMail).Alias, source.Email);
            }
        }
        #endregion
    }

}
