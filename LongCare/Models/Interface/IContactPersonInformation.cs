﻿using LongCare.Services;


namespace LongCare.Models.Interface
{
    interface IContactPersonInformation
    {
        string RealName { get; set; }
        string NickName { get; set; }
        EnumGenderType Gender { get; set; }
        EnumKinship Relationship { get; set; }
    }
}
