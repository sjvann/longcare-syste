﻿namespace LongCare.Models.Interface
{
    public interface IContactDataInformation
    {
        string Tel_Home { get; set; }
        string Tel_Office { get; set; }
        string Tel_Mobile { get; set; }
        string Email { get; set; }
    }
}
