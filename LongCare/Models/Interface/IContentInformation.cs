﻿namespace LongCare.Models.Interface
{
    public interface IContentInformation
    {
        string Title { get; set; }
        string MainImage { get; set; }
        string Description { get; set; }
    }
}
