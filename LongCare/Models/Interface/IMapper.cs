﻿
using Umbraco.Core.Models;

namespace LongCare.Models.Interface
{
    interface IMapper
    {
        T MapContent2ViewModel<T>(IContent content);
        void MapViewModel2Content<T>(T t, ref IContent content);

    }
}
