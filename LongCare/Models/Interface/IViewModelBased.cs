﻿namespace LongCare.Models.Interface
{
    public interface IViewModelBased
    {
        /// <summary>
        /// 目前頁面
        /// </summary>
         int? ContentId { get; set; }
        /// <summary>
        /// 目前頁面之父層
        /// </summary>
         int? ParentId { get; set; }
        /// <summary>
        /// 回轉頁面
        /// </summary>
         int? ReturnPageId { get; set; }
        /// <summary>
        /// 目前網站。使用於多國語言時。
        /// </summary>
         int? SiteId { get; set; }
        /// <summary>
        /// 目前登入帳號
        /// </summary>
        int? MemberId { get; set; }
    }
}