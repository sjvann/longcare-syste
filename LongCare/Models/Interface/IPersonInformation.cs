﻿
using LongCare.Services;
using System;
using System.Web;


namespace LongCare.Models.Interface
{
    public interface IPersonInformation
    {
        string PersonID { get; set; }
        string RealName { get; set; }
        string NickName { get; set; }
        EnumGenderType Gender { get; set; }
        DateTime Birthday { get; set; }
        string PictureUrl { get; set; }
        int PictureId { get; set; }
        HttpPostedFileBase Attachment { get; set; }
    }
}
