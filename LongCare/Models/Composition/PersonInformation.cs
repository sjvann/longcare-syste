﻿using LongCare.Models.Interface;
using LongCare.Services;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace LongCare.Models.Composition
{
    public class PersonInformation : IPersonInformation
    {
        //需求欄位
        [Required(ErrorMessage = "{0}是必要欄位")]
        [DisplayName("身分證號")]
        public string PersonID { get; set; }
        [Required(ErrorMessage = "{0}是必要欄位")]
        [DisplayName("住民姓名")]

        public string RealName { get; set; }
        [DisplayName("住民暱稱")]
        public string NickName { get; set; }

        [Required(ErrorMessage = "{0}是必要欄位")]
        [DisplayName("生日")]
        public DateTime Birthday { get; set; }

        [DisplayName("照片")]
        public string PictureUrl { get; set; }
        public int PictureId { get; set; }
        public HttpPostedFileBase Attachment { get; set; }
        [DisplayName("性別")]
        public EnumGenderType Gender { get; set; }
    }
}