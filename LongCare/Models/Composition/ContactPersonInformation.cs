﻿using LongCare.Models.Interface;
using LongCare.Services;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LongCare.Models.Composition
{
    public class ContactPersonInformation : IContactPersonInformation
    {
        public string PersonID { get; set; }
        [Required(ErrorMessage = "{0}是必要欄位")]
        [DisplayName("住民姓名")]

        public string RealName { get; set; }
        [DisplayName("住民暱稱")]
        public string NickName { get; set; }
        [DisplayName("性別")]
        public EnumGenderType Gender { get; set; }
        [Required(ErrorMessage = "{0}是必要欄位")]
        [DisplayName("關係")]
        public EnumKinship Relationship { get; set; }

        
    }
}