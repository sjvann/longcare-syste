﻿

using LongCare.Models.Interface;
using System.ComponentModel;

namespace LongCare.Models.Composition
{

    public class ContentInformation : IContentInformation
    {
        [DisplayName("標題")]
        public string Title { get; set; }
        [DisplayName("主影像")]
        public string MainImage { get; set; }
        [DisplayName("簡述")]
        public string Description { get; set; }
    }
}