﻿using LongCare.Models.Interface;
using System.ComponentModel;

namespace LongCare.Models.Composition
{
    public partial class ContactDataInformation : IContactDataInformation
    {
        [DisplayName("住家電話")]
        public string Tel_Home { get; set; }
        [DisplayName("辦公室電話")]
        public string Tel_Office { get; set; }
        [DisplayName("手機")]
        public string Tel_Mobile { get; set; }
        [DisplayName("電子郵件地址")]
        public string Email { get; set; }
    }
}