﻿using LongCare.Models.Composition;
using System.Collections.Generic;
using System.Web.Mvc;
namespace LongCare.Models.Resident
{
    public class MaintainResidentViewModel : ViewModelBased
    {
       public ContentInformation Content { get; set; }
        public PersonInformation Person { get; set; }
        public ContactDataInformation Contact { get; set; }
        public IEnumerable<SelectListItem> GenderList { get; set; }
        public override string IsRequired(string fieldName)
        {
            switch(fieldName)
            {
                case "PersonID":
                case "RealName":
                case "Birthday":
                    return base.IsRequired(fieldName);
                default:
                    return string.Empty;
            }

           
        }
    }
}