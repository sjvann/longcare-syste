﻿using LongCare.Models.Composition;
using System.Collections.Generic;
using System.Web.Mvc;
namespace LongCare.Models.Resident
{
    public class MaintainContactPersonViewModel: ViewModelBased
    {
       public ContentInformation Content { get; set; }
        public ContactPersonInformation Person { get; set; }
        public ContactDataInformation Contact { get; set; }
        public IEnumerable<SelectListItem> GenderList { get; set; }
        public IEnumerable<SelectListItem> RelationshipList { get; set; }
        public override string IsRequired(string fieldName)
        {
            switch(fieldName)
            {
              
                case "RealName":
                case "Relationship":
                    return base.IsRequired(fieldName);
                default:
                    return string.Empty;
            }

           
        }
    }
}