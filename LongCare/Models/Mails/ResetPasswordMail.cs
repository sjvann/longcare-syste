﻿using Postal;
using System.Collections.Generic;

namespace LongCare.Areas.LoginService.Models.Mails
{
    public class ResetPasswordMail : Email
    {
        public string To { get; set; }
        public string Subject { get; set; }
        public string Title { get; set; }
        public string UserName { get; set; }
        public int ReturnPageId { get; set; }
        public string ResetGuid { get; set; }
        public Dictionary<string, string> Message { get; set; }
    }
}
