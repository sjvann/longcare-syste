﻿using LongCare.Shared.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LongCare.Models.Account
{
    public class ForgotPasswordViewModel : ViewModelBasedWithMessage
    {
        [Required(ErrorMessage = "電子郵件地址為必填欄位")]
        [DisplayName("電子郵件地址")]
        [DataType(DataType.EmailAddress, ErrorMessage = "請輸入正確格式")]
        public string Email { get; set; }
        public override string IsRequired(string fieldName)
        {
            switch (fieldName)
            {
                case "Email":
                    return base.IsRequired(fieldName);
                default:
                    return string.Empty;
            }

        }

    }
}