﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LongCare.Models.Account
{
    public class LoginViewModel : ViewModelBased
    {
        [Required(ErrorMessage = "電子郵件帳號為必填欄位")]
        [DisplayName("帳號")]
        public string AccountName { get; set; }
        [Required(ErrorMessage = "密碼為必填欄位")]
        [DisplayName("密碼")]
        public string Passoword { get; set; }
        [DisplayName("記住我")]
        public bool RememberMe { get; set; }

        public override string IsRequired(string fieldName)
        {
            switch (fieldName)
            {
                case "AccountName":
                case "Passoword":
                    return base.IsRequired(fieldName);
                default:
                    return string.Empty;
            }
        }

    }
}