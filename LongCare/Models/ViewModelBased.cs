﻿using LongCare.Models.Interface;
using LongCare.Services;

namespace LongCare.Models
{
    public class ViewModelBased : IViewModelBased, IViewModelRequire
    {
        public int? ContentId { get; set; }
        public int? ParentId { get; set; }
        public int? ReturnPageId { get; set; }
        public int? SiteId { get; set; }

        /// <summary>
        /// 目前維護狀態：新增、編輯、刪除
        /// </summary>
        public EnumMaintainType MaintainType { get; set; }
        /// <summary>
        /// 目前登入者ID
        /// </summary>
        public int? MemberId { get; set; }
        /// <summary>
        /// 目前是否為根目錄節點
        /// </summary>
        public bool IsRoot { get; set; }

        public virtual string IsRequired(string fieldName)
        {
            return (fieldName != null) ? "<i class='fa fa-star text-danger' title='必填欄位'></i>" : string.Empty;
        }
    }
}
