﻿using LongCare.Models;
using System.Collections.Generic;


namespace LongCare.Shared.Models
{
    public class ViewModelBasedWithMessage :ViewModelBased
    {
        /// <summary>
        /// 要攜帶的訊息內容
        /// </summary>
        public Dictionary<string, string> Messages { get; set; }
    }
}